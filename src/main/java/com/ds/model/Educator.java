package com.ds.model;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@Entity
@Table(name = "educators")
@SequenceGenerator(name = "educators_gen", sequenceName = "educators_seq", allocationSize = 1)
public class Educator extends GenericModel{
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "person_id", foreignKey = @ForeignKey(name = "FK_EDUCATORS_PERSON"))
    private  Person person;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "group_id", foreignKey = @ForeignKey(name = "FK_EDUCATORS_GROUP"))
    private  Group group;

    @Override
    public String toString() {
        return "Educator{" +
                "id=" + id +
                "person=" + person +
                ", group=" + group +
                '}';
    }
}

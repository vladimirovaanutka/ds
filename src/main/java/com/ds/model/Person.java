package com.ds.model;

import com.ds.model.enums.Gender;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table(name = "persons")
@SequenceGenerator(name = "persons_gen", sequenceName = "persons_seq", allocationSize = 1)
public class Person extends GenericModel {
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "surname", nullable = false)
    private String surname;
    @Column(name = "patronymic")
    private String patronymic;
    @Column(name = "gender", nullable = false)
    @Enumerated
    private Gender gender;
    @Column(name = "birthDate", nullable = false)
    private LocalDate birthDate;
    @Column(name = "phone")
    private String phone;
    @Column(name = "is_child", nullable = false)
    private Boolean isChild;

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", gender=" + gender +
                ", birthDate=" + birthDate +
                ", phone='" + phone + '\'' +
                ", isChild=" + isChild +
                '}';
    }
}

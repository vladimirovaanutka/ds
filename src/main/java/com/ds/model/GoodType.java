package com.ds.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "goodTypes")
@SequenceGenerator(name = "goodTypes_gen", sequenceName = "goodTypes_seq", allocationSize = 1)
public class GoodType extends GenericModel{
    @Column(name = "goodTypeName", nullable = false)
    private String goodTypeName;

    @Override
    public String toString() {
        return "GoodType{" +
                "goodTypeName='" + goodTypeName + '\'' +
                '}';
    }
}

package com.ds.model;

import com.ds.model.enums.Gender;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "childrens")
@SequenceGenerator(name = "childrens_gen", sequenceName = "childrens_seq", allocationSize = 1)
public class Children extends GenericModel{
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "person_id", foreignKey = @ForeignKey(name = "FK_CHILDRENS_PERSON"))
    private  Person person;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "group_id", foreignKey = @ForeignKey(name = "FK_CHILDRENS_GROUP"))
    private  Group group;

   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "official_representative_id", foreignKey = @ForeignKey(name = "FK_CHILDRENS_OFFICIAL_REPRESENTATIVE"))
   private  OfficialRepresentative officialRepresentative;

    @Override
    public String toString() {
        return "Children{" +
                "person=" + person +
//                ", age=" + age +
                ", group=" + group +
                ", officialRepresentative=" + officialRepresentative +
                '}';
    }
}

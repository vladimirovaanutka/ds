package com.ds.model;

import com.ds.model.enums.GroupType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "ds_groups")
@SequenceGenerator(name = "groups_gen", sequenceName = "groups_seq", allocationSize = 1)
public class Group extends GenericModel {
    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "group", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Educator> educatorSet;

    @Column(name = "groupType", nullable = false)
    @Enumerated
    private GroupType groupType;

    @OneToMany(mappedBy = "group", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Children> childrenSet;

    @Override
    public String toString() {
        return "Group{" +
                "name='" + name + '\'' +
                //", educator=" + educatorSet +
                ", groupType=" + groupType +
                //", children=" + childrenSet +
                '}';
    }
}

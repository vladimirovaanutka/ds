package com.ds.model;

import com.ds.model.enums.Status;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table(name = "ds_orders")
@SequenceGenerator(name = "orders_gen", sequenceName = "orders_seq", allocationSize = 1)
public class Order extends GenericModel {
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn (name = "educator_id", nullable = false,
//            foreignKey = @ForeignKey(name = "FK_ORDERS_EDUCATOR"))
//    private  Educator educator;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn (name = "goodType_id", nullable = false,
            foreignKey = @ForeignKey(name = "FK_ORDERS_GOOD_TYPE"))
    private  GoodType goodType;

    @Column(name = "description")
    private String description;

    @Column(name = "status", nullable = false)
    @Enumerated
    private Status status;

    @Column(name = "quantity")
    private Integer quantity;

    @Override
    public String toString() {
        return "Order{" +
                //"educator=" + educator +
                ", description='" + description + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}

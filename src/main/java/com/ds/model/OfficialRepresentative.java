package com.ds.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "official_representatives")
@SequenceGenerator(name = "official_representatives_gen", sequenceName = "official_representatives_seq", allocationSize = 1)
public class OfficialRepresentative extends GenericModel{
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "person_id", foreignKey = @ForeignKey(name = "FK_OFFICIAL_REPRESENTATIVES_PERSON"))
    private  Person person;
    @OneToMany(mappedBy = "officialRepresentative", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Children> childrenSet;

    @Column(name = "address")

    private String address;

    @Column(name = "email")
    private String email;

    @Override
    public String toString() {
        return "OfficialRepresentative{" +
                "person=" + person +
                ", children=" + childrenSet +
                ", address='" + address + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}

package com.ds.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Entity
@Table(name = "ds_users")
@SequenceGenerator(name = "default_gen", sequenceName = "users_seq", allocationSize = 1)
public class User extends GenericModel{
  /*  @Column(name = "login", nullable = false)
    private String login;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "change_password_token")
    private String changePasswordToken;*/

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "person_id", foreignKey = @ForeignKey(name = "FK_USERS_PERSON"))
    private  Person person;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn (name = "role_id", nullable = false, foreignKey = @ForeignKey(name = "FK_USERS_ROLE"))
    private  Role role;

    @Override
    public String toString() {
        return "User{" +
                "person=" + person +
                ", role=" + role +
                '}';
    }
}

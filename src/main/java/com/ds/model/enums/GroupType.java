package com.ds.model.enums;

public enum GroupType {
    NURSERY("Ясли"),
    YOUNGER_GROUP("Младшая группа"),
    MIDDLE_GROUP("Средняя группа"),
    SENIOR_GROUP("Старшая группа"),
    PRESCHOOL_GROUP("Подготовительная группа");




    private final String typeTextDisplay;

    GroupType(String text) {
        this.typeTextDisplay = text;
    }

    public String getTypeTextDisplay() {
        return this.typeTextDisplay;
    }

}


package com.ds.model.enums;

public enum Status {
    CREATED("Создано"),
    SENTED ("Отправлено"),
    APPROVED ("Одобрено"),
    REJECTED ("Отклонено"),
    DONE ("Выполнено");

    private final String statusTextDisplay;

    Status(String text) {
        this.statusTextDisplay = text;
    }

    public String getStatusTextDisplay() {
        return this.statusTextDisplay;
    }

}

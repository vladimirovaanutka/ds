package com.ds.model.enums;

public enum Gender {
    MALE("Мужской"),
    FEMALE("Женский");

    private final String genderTextDisplay;

    Gender(String text) {
        this.genderTextDisplay = text;
    }

    public String getGenderTextDisplay() {
        return this.genderTextDisplay;
    }

}

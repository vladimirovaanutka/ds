package com.ds.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "storage")
@SequenceGenerator(name = "storage_gen", sequenceName = "storage_seq", allocationSize = 1)
public class Storage extends GenericModel{
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn (name = "goodType_id", nullable = false,
            foreignKey = @ForeignKey(name = "FK_STORAGES_GOOD_TYPE"))
    private  GoodType goodType;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "quantity", nullable = false)
    private Integer quantity;

    @Column(name = "comments")
    private String comments;
}

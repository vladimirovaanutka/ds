package com.ds.MVC.controller;

import com.ds.dto.GoodTypeDto;
import com.ds.mapper.GoodTypeMapper;
import com.ds.model.GoodType;
import com.ds.service.GoodTypeService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/good/types")
@Slf4j
public class MVCGoodTypeController {
    private final GoodTypeMapper goodTypeMapper;
    private final GoodTypeService goodTypeService;


    public MVCGoodTypeController(GoodTypeMapper goodTypeMapper, GoodTypeService goodTypeService) {
        this.goodTypeMapper = goodTypeMapper;
        this.goodTypeService = goodTypeService;
    }

    @GetMapping("")
    public String getAll(
            @RequestParam(value = "page", defaultValue = "1") int page,
            @RequestParam(value = "size", defaultValue = "5") int pageSize,
            Model model
    ) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "goodTypeName"));
        Page<GoodType> goodTypePage = goodTypeService.listAll(pageRequest);
        Page<GoodTypeDto> goodTypeDtoPage = new PageImpl<>(goodTypeMapper.toDtos(goodTypePage.getContent()), pageRequest, goodTypePage.getTotalElements());
        model.addAttribute("goodTypes", goodTypeDtoPage);
        return "goodTypes/viewAllGoodTypes";
    }
    @GetMapping("/add")
    public String create(@ModelAttribute("goodTypeForm") GoodTypeDto goodTypeDto) {
        return "goodTypes/addGoodTypes";
    }

    @PostMapping("/add")
    public String create(
            @ModelAttribute("goodTypeForm") @Valid GoodTypeDto goodTypeDto,
            BindingResult result
    ) {
        if(result.hasErrors()) {
            return "/goodTypes/addGoodTypes";
        } else {
            goodTypeService.create(goodTypeMapper.toEntity(goodTypeDto));
            return "redirect:/good/types";
        }
    }
    @GetMapping("/delete/{id}")
    public String safeDelete(@PathVariable Integer id) {
        goodTypeService.softDelete(id);
        return "redirect:/good/types";
    }
}

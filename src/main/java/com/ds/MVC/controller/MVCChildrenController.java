package com.ds.MVC.controller;

import com.ds.dto.*;
import com.ds.mapper.*;
import com.ds.model.Children;
import com.ds.model.OfficialRepresentative;
import com.ds.model.Person;
import com.ds.service.ChildrenService;
import com.ds.service.GroupService;
import com.ds.service.OfficialRepresentativeService;
import com.ds.service.PersonService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/childrens")
@Slf4j
public class MVCChildrenController {

    private final ChildrenFullMapper childrenFullMapper;
    private final ChildrenMapper childrenMapper;
    private final PersonService personService;
    private final ChildrenService childrenService;
    private final PersonMapper personMapper;
    private final GroupService groupService;
    private final GroupMapper groupMapper;
    private final OfficialRepresentativeService officialRepresentativeService;
    private final OfficialRepresentativeMapper officialRepresentativeMapper;
    private final OfficialRepresentativeFullMapper officialRepresentativeFullMapper;

    public MVCChildrenController(ChildrenFullMapper childrenFullMapper, PersonService personService, ChildrenService childrenService,
                                 PersonMapper personMapper, GroupService groupService, GroupMapper groupMapper,
                                 ChildrenMapper childrenMapper, OfficialRepresentativeService officialRepresentativeService,
                                 OfficialRepresentativeMapper officialRepresentativeMapper, OfficialRepresentativeFullMapper officialRepresentativeFullMapper) {
        this.childrenFullMapper = childrenFullMapper;
        this.personService = personService;
        this.childrenService = childrenService;
        this.personMapper = personMapper;
        this.groupService = groupService;
        this.groupMapper = groupMapper;
        this.childrenMapper = childrenMapper;
        this.officialRepresentativeService = officialRepresentativeService;
        this.officialRepresentativeMapper = officialRepresentativeMapper;
        this.officialRepresentativeFullMapper = officialRepresentativeFullMapper;
    }

    @GetMapping("")
    public String getAll(
            @RequestParam(value = "page", defaultValue = "1") int page,
            @RequestParam(value = "size", defaultValue = "5") int pageSize,
            Model model
    ) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "group.groupType", "person.surname", "person.name", "person.patronymic"));
        Page<Children> childrenPage = childrenService.listAll(pageRequest);
        Page<ChildrenFullDto> childrenDtoPage = new PageImpl<>(childrenFullMapper.toDtos(childrenPage.getContent()), pageRequest, childrenPage.getTotalElements());
        model.addAttribute("childrens", childrenDtoPage);
        model.addAttribute("personService", personService);
        return "childrens/viewAllChildrens";
    }

    @GetMapping("/{id}")
    public String getOne(@PathVariable Integer id, Model model) {
        model.addAttribute("children", childrenFullMapper.toDto(childrenService.getOne(id)));
        model.addAttribute("personService", personService);
        return "childrens/viewChildren";
    }

    @GetMapping("/representatives/{id}")
    public String getOneRepresentatives(@PathVariable Integer id, Model model) {
        model.addAttribute("representative", officialRepresentativeFullMapper.toDto(officialRepresentativeService.getOne(id)));
        model.addAttribute("personService", personService);
        model.addAttribute("childrenService", childrenService);
        return "childrens/viewRepresentative";
    }

    @GetMapping("/add/current")
    public String createCurrent(@ModelAttribute("childrenForm") ChildrenFullDto childrenDto, Model model) {
        model.addAttribute("adultPersons", personMapper.toDtos(personService.getAdultPersons()));
        model.addAttribute("groups", groupMapper.toDtos(groupService.listAll()));
        model.addAttribute("personService", personService);
        return "childrens/addCurrentChildren";
    }

    @GetMapping("/add/new")
    public String createNew(@ModelAttribute("childrenForm") ChildrenFullDto childrenDto, Model model) {
        OfficialRepresentativeFullDto officialRepresentativeFullDto = new OfficialRepresentativeFullDto();
        officialRepresentativeFullDto.setPersonDto(new PersonDto());
        childrenDto.setOfficialRepresentativeDto(officialRepresentativeFullDto);
        model.addAttribute("groups", groupMapper.toDtos(groupService.listAll()));
        model.addAttribute("personService", personService);
        return "childrens/addNewChildren";
    }

    @PostMapping("/add/current")
    public String createCurrent(
            @ModelAttribute("childrenForm") @Valid ChildrenFullDto childrenDto,
            BindingResult result
    ) {
        if(result.hasErrors()) {
            //todo добавить валидацию, что такая комбинация уже существует
            return "/childrens/addCurrentChildren";
        } else {
            childrenDto.getPersonDto().setIsChild(true);
            Person childrenPerson = personService.create(personMapper.toEntity(childrenDto.getPersonDto()));
            childrenDto.setPersonId(childrenPerson.getId());
            // получаем предствителя по идентификатору физлица
            // если представителя ранее не было и он создался из физлица, то нужно потом досоздать ему адрес и почту
            OfficialRepresentative officialRepresentative =
                    officialRepresentativeService.getOfficialRepresentativeByPersonId(
                            childrenDto.getOfficialRepresentativePersonId());
            childrenDto.setOfficialRepresentativeId(officialRepresentative.getId());
            childrenService.create(childrenMapper.toEntity(childrenDto));
            return "redirect:/childrens";
        }
    }

    @PostMapping("/add/new")
    public String createNew(
            @ModelAttribute("childrenForm") @Valid ChildrenFullDto childrenDto,
            BindingResult result
    ) {
        if(result.hasErrors()) {
            //todo добавить валидацию, что такая комбинация уже существует
            return "/childrens/addNewChildren";
        } else {
            childrenDto.getPersonDto().setIsChild(true);
            Person childrenPerson = personService.create(personMapper.toEntity(childrenDto.getPersonDto()));
            childrenDto.setPersonId(childrenPerson.getId());

            childrenDto.getOfficialRepresentativeDto().getPersonDto().setIsChild(false);
            Person officialRepresentativePerson = personService.create(personMapper.toEntity(childrenDto.getOfficialRepresentativeDto().getPersonDto()));
            childrenDto.getOfficialRepresentativeDto().setPersonId(officialRepresentativePerson.getId());
            OfficialRepresentative officialRepresentative =
                    officialRepresentativeService.create(officialRepresentativeMapper.toEntity(
                            childrenDto.getOfficialRepresentativeDto()));
            childrenDto.setOfficialRepresentativeId(officialRepresentative.getId());
            childrenService.create(childrenMapper.toEntity(childrenDto));
            return "redirect:/childrens";
        }
    }

    @GetMapping("/delete/{id}")
    public String safeDelete(@PathVariable Integer id) {
        ChildrenFullDto childrenFullDto = childrenFullMapper.toDto(childrenService.getOne(id));
        childrenService.softDelete(id);
        //todo добавить проверку на представителя и персона (ниже). Удалять тех, кто не нужен.
        if (personService.isAdultPersonCanDelete(childrenFullDto.getPersonDto())) {
            personService.softDelete(childrenFullDto.getPersonDto().getId());
        }

        return "redirect:/childrens";
    }

    @GetMapping("/update/{id}")
    public String update(Model model, @PathVariable Integer id) {
        model.addAttribute("children", childrenFullMapper.toDto(childrenService.getOne(id)));
        model.addAttribute("adultPersons", personMapper.toDtos(personService.getAdultPersons()));
        model.addAttribute("personService", personService);
        model.addAttribute("groups", groupMapper.toDtos(groupService.listAll()));
        return "childrens/updateChildren";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("childrenForm") ChildrenFullDto childrenFullDto) {
        Person person = personService.update(personMapper.toEntity(childrenFullDto.getPersonDto()));
        childrenFullDto.setPersonId(person.getId());
        // получаем предствителя по идентификатору физлица
        // если представителя ранее не было и он создался из физлица, то нужно потом досоздать ему адрес и почту
        OfficialRepresentative officialRepresentative =
                officialRepresentativeService.getOfficialRepresentativeByPersonId(
                        childrenFullDto.getOfficialRepresentativePersonId());
        childrenFullDto.setOfficialRepresentativeId(officialRepresentative.getId());
        childrenService.update(childrenFullMapper.toEntity(childrenFullDto));
        return "redirect:/childrens";
    }

    @GetMapping("/representative/update/{id}")
    public String updateRepresentative(Model model, @PathVariable Integer id) {
        model.addAttribute("representative", officialRepresentativeFullMapper.toDto(officialRepresentativeService.getOne(id)));
        model.addAttribute("personService", personService);
        return "childrens/updateRepresentative";
    }

    @PostMapping("/representative/update")
    public String updateRepresentative(@ModelAttribute("representativeForm") OfficialRepresentativeFullDto representativeFullDto) {
        Person person = personService.update(personMapper.toEntity(representativeFullDto.getPersonDto()));
        representativeFullDto.setPersonId(person.getId());
        officialRepresentativeService.update(officialRepresentativeFullMapper.toEntity(representativeFullDto));
        return "redirect:/childrens";
    }

}

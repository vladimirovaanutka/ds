package com.ds.MVC.controller;

import com.ds.dto.CreatePersonDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/persons")
@Slf4j
public class MVCPersonController {

    @GetMapping("/add/{type}")
    public String create(@ModelAttribute("personForm") CreatePersonDto createPersonDto,
                         @PathVariable String type,
                         Model model) {
        return "persons/addPerson";
    }

}

package com.ds.MVC.controller;

import com.ds.dto.GroupDto;
import com.ds.mapper.GroupMapper;
import com.ds.mapper.GroupFullMapper;
import com.ds.model.Group;
import com.ds.service.ChildrenService;
import com.ds.service.EducatorService;
import com.ds.service.GroupService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/groups")
@Slf4j
public class MVCGroupController {
    private final GroupService groupService;
    private final GroupMapper groupMapper;
    private final EducatorService educatorService;
    private final ChildrenService childrenService;
    private final GroupFullMapper groupFullMapper;

    public MVCGroupController(GroupService groupService, GroupMapper groupMapper, EducatorService educatorService, ChildrenService childrenService, GroupFullMapper groupFullMapper) {
        this.groupService = groupService;
        this.groupMapper = groupMapper;
        this.educatorService = educatorService;
        this.childrenService = childrenService;
        this.groupFullMapper = groupFullMapper;
    }

    @GetMapping("")
    public String getAll(
            @RequestParam(value = "page", defaultValue = "1") int page,
            @RequestParam(value = "size", defaultValue = "5") int pageSize,
            Model model
    ) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "name"));
        Page<Group> groupPage = groupService.listAll(pageRequest);
        Page<GroupDto> groupDtoPage = new PageImpl<>(groupMapper.toDtos(groupPage.getContent()), pageRequest, groupPage.getTotalElements());
        model.addAttribute("groups", groupDtoPage);
        return "groups/viewAllGroups";
    }

    @GetMapping("/{id}")
    public String getOne(@PathVariable Integer id, Model model) {
        model.addAttribute("group", groupFullMapper.toDto(groupService.getOne(id)));
        return "groups/viewGroup";
    }
    @GetMapping("/add")
    public String create(@ModelAttribute("groupForm") GroupDto groupDto) {
        return "groups/addGroup";
    }

    @PostMapping("/add")
    public String create(
            @ModelAttribute("groupForm") @Valid GroupDto groupDto,
            BindingResult result
    ) {
        if(result.hasErrors()) {
            return "/groups/addGroup";
        } else {
            groupService.create(groupMapper.toEntity(groupDto));
            return "redirect:/groups";
        }
    }

    @GetMapping("/delete/{id}")
    public String safeDelete(@PathVariable Integer id) {
        groupService.softDelete(id);
        return "redirect:/groups";
    }

    @GetMapping("/update/{id}")
    public String update(Model model, @PathVariable Integer id) {
        model.addAttribute("group", groupMapper.toDto(groupService.getOne(id)));
        return "groups/updateGroup";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("groupForm") GroupDto groupDto) {
        groupService.update(groupMapper.toEntity(groupDto));
        return "redirect:/groups";
    }



//    @GetMapping("/restore/{id}")
//    public String restore(@PathVariable Integer id) {
//        groupService.restore(id);
//        return "redirect:/groups";
//    }
}

package com.ds.MVC.controller;

import com.ds.dto.EducatorFullDto;
import com.ds.dto.OrderDto;
import com.ds.dto.OrderFullDto;
import com.ds.mapper.GoodTypeMapper;
import com.ds.mapper.OrderFullMapper;
import com.ds.mapper.OrderMapper;
import com.ds.model.Order;
import com.ds.model.Person;
import com.ds.model.enums.Status;
import com.ds.service.EducatorService;
import com.ds.service.GoodTypeService;
import com.ds.service.OrderService;
import com.ds.service.PersonService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/orders")
@Slf4j
public class MVCOrderController {
    private final OrderMapper orderMapper;
    private final OrderService orderService;
    private final OrderFullMapper orderFullMapper;
    private final EducatorService educatorService;
    private final PersonService personService;
    private final GoodTypeService goodTypeService;
    private final GoodTypeMapper goodTypeMapper;

    public MVCOrderController(OrderMapper orderMapper, OrderService orderService, OrderFullMapper orderFullMapper,
                              EducatorService educatorService, GoodTypeService goodTypeService,
                              GoodTypeMapper goodTypeMapper, PersonService personService) {
        this.orderMapper = orderMapper;
        this.orderService = orderService;
        this.orderFullMapper = orderFullMapper;
        this.educatorService = educatorService;
        this.goodTypeService = goodTypeService;
        this.goodTypeMapper = goodTypeMapper;
        this.personService = personService;
    }
    @GetMapping("")
    public String getAll(
            @RequestParam(value = "page", defaultValue = "1") int page,
            @RequestParam(value = "size", defaultValue = "5") int pageSize,
            Model model
    ) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "id"));
        Page<Order> orderPage = orderService.listAll(pageRequest);
        Page<OrderFullDto> orderDtoPage = new PageImpl<>(orderFullMapper.toDtos(orderPage.getContent()), pageRequest, orderPage.getTotalElements());
        model.addAttribute("orders", orderDtoPage);
        return "orders/viewAllOrders";
    }

    @GetMapping("/{id}")
    public String getOne(@PathVariable Integer id, Model model) {
        model.addAttribute("order", orderFullMapper.toDto(orderService.getOne(id)));
        model.addAttribute("personService", personService);
        return "orders/viewOrder";
    }
    @GetMapping("/add")
    public String create(@ModelAttribute("orderForm") OrderFullDto orderFullDto, Model model) {
        model.addAttribute("goodTypes", goodTypeMapper.toDtos(goodTypeService.listAll()));
        return "orders/addOrder";
    }

    @PostMapping("/add")
    public String create(
            @ModelAttribute("orderForm") @Valid OrderFullDto orderFullDto,
            BindingResult result
    ) {
        if(result.hasErrors()) {
            return "/orders/addOrder";
        } else {
            orderFullDto.setStatus(Status.CREATED);
            orderService.create(orderMapper.toEntity(orderFullDto));
            return "redirect:/orders";
        }
    }

    @GetMapping("/delete/{id}")
    public String safeDelete(@PathVariable Integer id) {
        orderService.softDelete(id);
        return "redirect:/orders";
    }

    @GetMapping("/update/{id}")
    public String update(Model model, @PathVariable Integer id) {
        model.addAttribute("order", orderFullMapper.toDto(orderService.getOne(id)));
        model.addAttribute("goodTypes", goodTypeMapper.toDtos(goodTypeService.listAll()));
        return "orders/updateOrder";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("educatorForm") OrderFullDto orderFullDto) {
        orderService.update(orderFullMapper.toEntity(orderFullDto));
        return "redirect:/orders";
    }
}

package com.ds.MVC.controller;

import com.ds.dto.GroupDto;
import com.ds.dto.OrderFullDto;
import com.ds.dto.StorageDto;
import com.ds.dto.StorageFullDto;
import com.ds.mapper.*;
import com.ds.model.Group;
import com.ds.model.Storage;
import com.ds.model.enums.Status;
import com.ds.service.*;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/storages")
@Slf4j
public class MVCStorageController {
    private final StorageService storageService;
    private final StorageMapper storageMapper;
    private final StorageFullMapper storageFullMapper;
    private final GoodTypeService goodTypeService;
    private final GoodTypeMapper goodTypeMapper;

    public MVCStorageController (StorageService storageService, StorageMapper storageMapper, StorageFullMapper storageFullMapper,
                                 GoodTypeService goodTypeService, GoodTypeMapper goodTypeMapper) {
        this.storageService = storageService;
        this.storageMapper = storageMapper;
        this.storageFullMapper = storageFullMapper;
        this.goodTypeService = goodTypeService;
        this.goodTypeMapper = goodTypeMapper;
    }

    @GetMapping("")
    public String getAll(
            @RequestParam(value = "page", defaultValue = "1") int page,
            @RequestParam(value = "size", defaultValue = "5") int pageSize,
            Model model
    ) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "name"));
        Page<Storage> storagePage = storageService.listAll(pageRequest);
        Page<StorageFullDto> storageDtoPage = new PageImpl<>(storageFullMapper.toDtos(storagePage.getContent()), pageRequest, storagePage.getTotalElements());
        model.addAttribute("storages", storageDtoPage);
        return "storages/viewAllStorages";
    }

    @GetMapping("/add")
    public String create(@ModelAttribute("storageForm") StorageFullDto storageFullDto, Model model) {
        model.addAttribute("goodTypes", goodTypeMapper.toDtos(goodTypeService.listAll()));
        return "storages/addStorage";
    }

    @PostMapping("/add")
    public String create(
            @ModelAttribute("storageForm") @Valid StorageFullDto storageFullDto,
            BindingResult result
    ) {
        if(result.hasErrors()) {
            return "/storages/addStorage";
        } else {
            storageService.create(storageFullMapper.toEntity(storageFullDto));
            return "redirect:/storages";
        }
    }
}

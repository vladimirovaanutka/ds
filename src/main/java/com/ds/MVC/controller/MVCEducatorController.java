package com.ds.MVC.controller;

import com.ds.dto.EducatorDto;
import com.ds.dto.EducatorFullDto;
import com.ds.dto.PersonDto;
import com.ds.mapper.EducatorMapper;
import com.ds.mapper.EducatorFullMapper;
import com.ds.mapper.GroupMapper;
import com.ds.mapper.PersonMapper;
import com.ds.model.Educator;
import com.ds.model.Person;
import com.ds.service.EducatorService;
import com.ds.service.GroupService;
import com.ds.service.PersonService;
import jakarta.validation.Valid;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/educators")
@Slf4j
public class MVCEducatorController {
    private final EducatorFullMapper educatorFullMapper;
    private final EducatorMapper educatorMapper;
    private final PersonMapper personMapper;
    private final EducatorService educatorService;
    private final GroupService groupService;
    private final GroupMapper groupMapper;
    private final PersonService personService;

    public MVCEducatorController(EducatorFullMapper educatorFullMapper, EducatorMapper educatorMapper,
                                 EducatorService educatorService, GroupService groupService, PersonService personService,
                                 PersonMapper personMapper, GroupMapper groupMapper) {
        this.educatorService = educatorService;
        this.groupService = groupService;
        this.personService = personService;
        this.educatorMapper = educatorMapper;
        this.educatorFullMapper = educatorFullMapper;
        this.personMapper = personMapper;
        this.groupMapper = groupMapper;
    }

    @GetMapping("")
    public String getAll(
            @RequestParam(value = "page", defaultValue = "1") int page,
            @RequestParam(value = "size", defaultValue = "5") int pageSize,
            Model model
    ) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "person.surname", "person.name", "person.patronymic"));
        Page<Educator> educatorPage = educatorService.listAll(pageRequest);
        Page<EducatorFullDto> educatorDtoPage = new PageImpl<>(educatorFullMapper.toDtos(educatorPage.getContent()), pageRequest, educatorPage.getTotalElements());
        model.addAttribute("educators", educatorDtoPage);
        model.addAttribute("personService", personService);

//        model.addAttribute("educatorFormData", EducatorFormData.builder()
//                .addNewEducator(true)
//                .build());
        return "educators/viewAllEducators";
    }
    @GetMapping("/{id}")
    public String getOne(@PathVariable Integer id, Model model) {
        model.addAttribute("educator", educatorFullMapper.toDto(educatorService.getOne(id)));
        model.addAttribute("personService", personService);
        return "educators/viewEducator";
    }

    @GetMapping("/add/current")
    public String create(@ModelAttribute("educatorForm") EducatorDto educatorDto, Model model) {
        model.addAttribute("adultPersons", personMapper.toDtos(personService.getAdultPersons()));
        model.addAttribute("groups", groupMapper.toDtos(groupService.listAll()));
        model.addAttribute("personService", personService);
        return "educators/addCurrentEducator";
    }

    @GetMapping("/add/new")
    public String create(@ModelAttribute("educatorForm") EducatorFullDto educatorDto, Model model) {
        educatorDto.setPersonDto(new PersonDto());
        model.addAttribute("groups", groupMapper.toDtos(groupService.listAll()));
        model.addAttribute("personService", personService);
        return "educators/addNewEducator";
    }

    @PostMapping("/add/current")
    public String create(
            @ModelAttribute("educatorForm") @Valid EducatorDto educatorDto,
            BindingResult result
    ) {
        if(result.hasErrors()) {
            //todo добавить валидацию, что такая комбинация уже существует
            return "/educators/addCurrentEducator";
        } else {
            educatorService.create(educatorMapper.toEntity(educatorDto));
            return "redirect:/educators";
        }
    }

    @PostMapping("/add/new")
    public String create(
            @ModelAttribute("educatorForm") @Valid EducatorFullDto educatorDto,
            BindingResult result
    ) {
        if(result.hasErrors()) {
            //todo добавить валидацию, что такое физлицо уже существует
            return "/educators/addNewEducator";
        } else {
            educatorDto.getPersonDto().setIsChild(false);
            Person person = personService.create(personMapper.toEntity(educatorDto.getPersonDto()));
            educatorDto.setPersonId(person.getId());
            educatorService.create(educatorMapper.toEntity(educatorDto));
            return "redirect:/educators";
        }
    }

    @GetMapping("/delete/{id}")
    public String safeDelete(@PathVariable Integer id) {
        EducatorFullDto educatorDto = educatorFullMapper.toDto(educatorService.getOne(id));
        educatorService.softDelete(id);
        if (personService.isAdultPersonCanDelete(educatorDto.getPersonDto())) {
            personService.softDelete(educatorDto.getPersonDto().getId());
        }

        return "redirect:/educators";
    }

    @GetMapping("/update/{id}")
    public String update(Model model, @PathVariable Integer id) {
        model.addAttribute("educator", educatorFullMapper.toDto(educatorService.getOne(id)));
        model.addAttribute("personService", personService);
        model.addAttribute("groups", groupMapper.toDtos(groupService.listAll()));
        return "educators/updateEducator";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("educatorForm") EducatorFullDto educatorFullDto) {
        Person person = personService.update(personMapper.toEntity(educatorFullDto.getPersonDto()));
        educatorFullDto.setPersonId(person.getId());
        educatorService.update(educatorFullMapper.toEntity(educatorFullDto));
        return "redirect:/educators";
    }
}
package com.ds.mapper;

import com.ds.dto.PersonDto;
import com.ds.model.Person;
import com.ds.repository.PersonRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class PersonMapper extends GenericMapper<Person, PersonDto>{

    private final PersonRepository personRepository;

    protected PersonMapper(ModelMapper modelMapper, PersonRepository personRepository) {
        super(modelMapper, Person.class, PersonDto.class);
        this.personRepository = personRepository;
    }

    @Override
    protected void mapSpecificFields(PersonDto source, Person destination) {

    }

    @Override
    protected void mapSpecificFields(Person source, PersonDto destination) {
        //destination.setFio(getFio(source));
    }

//    private String getFio(Person person) {
//        return String.format("%s %s %s", person.getSurname(), person.getName(), person.getPatronymic());
//    }
}

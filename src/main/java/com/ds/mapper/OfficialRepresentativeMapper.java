package com.ds.mapper;

import com.ds.dto.OfficialRepresentativeDto;
import com.ds.model.GenericModel;
import com.ds.model.OfficialRepresentative;
import com.ds.repository.ChildrenRepository;
import com.ds.repository.PersonRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class OfficialRepresentativeMapper extends GenericMapper<OfficialRepresentative, OfficialRepresentativeDto> {
    private final PersonRepository personRepository;

    private final ChildrenRepository childrenRepository;

    protected OfficialRepresentativeMapper(ModelMapper modelMapper, PersonRepository personRepository, ChildrenRepository childrenRepository) {
        super(modelMapper, OfficialRepresentative.class, OfficialRepresentativeDto.class);
        this.personRepository = personRepository;
        this.childrenRepository = childrenRepository;
    }

    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(OfficialRepresentative.class, OfficialRepresentativeDto.class)
                .addMappings(m -> m.skip(OfficialRepresentativeDto::setChildrensIds)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(OfficialRepresentativeDto::setPersonId)).setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(OfficialRepresentativeDto.class, OfficialRepresentative.class)
                .addMappings(m -> m.skip(OfficialRepresentative::setChildrenSet)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(OfficialRepresentative::setPerson)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(OfficialRepresentativeDto source, OfficialRepresentative destination) {
        if (!Objects.isNull(source.getChildrensIds())) {
            destination.setChildrenSet(new HashSet<>(childrenRepository.findAllById(source.getChildrensIds())));
        }
        destination.setPerson(personRepository.findById(source.getPersonId())
                .orElseThrow(() -> new NotFoundException("Данные не найдены")));
    }

    @Override
    protected void mapSpecificFields(OfficialRepresentative source, OfficialRepresentativeDto destination) {
        destination.setChildrensIds(getChildrenId(source));
        destination.setPersonId(source.getPerson().getId());
    }

    protected Set<Integer> getChildrenId(OfficialRepresentative entity) {
        return Objects.isNull(entity.getChildrenSet())
                ? null
                : entity.getChildrenSet()
                .stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}

package com.ds.mapper;

import com.ds.dto.*;
import com.ds.model.Children;
import com.ds.model.Educator;
import com.ds.model.OfficialRepresentative;
import com.ds.repository.GroupRepository;
import com.ds.repository.OfficialRepresentativeRepository;
import com.ds.repository.PersonRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;

@Component
public class ChildrenFullMapper extends GenericMapper<Children, ChildrenFullDto> {
    private final ModelMapper mapper;
    private final PersonRepository personRepository;
    private final GroupRepository groupRepository;
    private final OfficialRepresentativeRepository officialRepresentativeRepository;

    public ChildrenFullMapper(ModelMapper modelMapper, ModelMapper mapper,
                              PersonRepository personRepository, GroupRepository groupRepository,
                              OfficialRepresentativeRepository officialRepresentativeRepository) {
        super(modelMapper, Children.class, ChildrenFullDto.class);
        this.mapper = mapper;
        this.personRepository = personRepository;
        this.groupRepository = groupRepository;
        this.officialRepresentativeRepository = officialRepresentativeRepository;
    }

    @PostConstruct
    public void setupMapper() {
        mapper.createTypeMap(Children.class, ChildrenFullDto.class)
                .addMappings(m -> m.skip(ChildrenFullDto::setPersonId))
                .addMappings(m -> m.skip(ChildrenFullDto::setGroupId))
                .addMappings(m -> m.skip(ChildrenFullDto::setOfficialRepresentativeId))
                .setPostConverter(toDtoConverter());
        mapper.createTypeMap(ChildrenFullDto.class, Children.class)
                .addMappings(m -> m.skip(Children::setPerson))
                .addMappings(m -> m.skip(Children::setGroup))
                .addMappings(m -> m.skip(Children::setOfficialRepresentative))
                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(ChildrenFullDto source, Children destination) {
        destination.setPerson(personRepository.findById(source.getPersonId())
                .orElseThrow(() -> new NotFoundException("Данные Person не найдены")));
        destination.setGroup(groupRepository.findById(source.getGroupId())
                .orElseThrow(() -> new NotFoundException("Данные Group не найдены")));
        destination.setOfficialRepresentative(officialRepresentativeRepository.findById(source.getOfficialRepresentativeId())
                .orElseThrow(() -> new NotFoundException("Данные officialRepresentative не найдены")));
    }

    @Override
    protected void mapSpecificFields(Children source, ChildrenFullDto destination) {
        destination.setPersonId(source.getPerson().getId());
        destination.setPersonDto(modelMapper.map(source.getPerson(), PersonDto.class));
        destination.setGroupId(source.getGroup().getId());
        destination.setGroupDto(modelMapper.map(source.getGroup(), GroupDto.class));
        destination.setOfficialRepresentativeId(source.getOfficialRepresentative().getId());
        destination.setOfficialRepresentativeDto(modelMapper.map(source.getOfficialRepresentative(), OfficialRepresentativeFullDto.class));
    }
}

package com.ds.mapper;

import com.ds.dto.GoodTypeDto;
import com.ds.model.GoodType;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class GoodTypeMapper extends GenericMapper<GoodType, GoodTypeDto>{
    protected GoodTypeMapper(ModelMapper modelMapper) {
        super(modelMapper, GoodType.class, GoodTypeDto.class );
    }

    @Override
    protected void mapSpecificFields(GoodTypeDto source, GoodType destination) {

    }

    @Override
    protected void mapSpecificFields(GoodType source, GoodTypeDto destination) {

    }
}

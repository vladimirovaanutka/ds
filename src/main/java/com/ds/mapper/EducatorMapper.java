package com.ds.mapper;

import com.ds.dto.EducatorDto;
import com.ds.model.Educator;
import com.ds.repository.GroupRepository;
import com.ds.repository.PersonRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;

@Component
public class EducatorMapper extends GenericMapper<Educator, EducatorDto> {
    private final PersonRepository personRepository;
    private final GroupRepository groupRepository;

    protected EducatorMapper(ModelMapper modelMapper, PersonRepository personRepository, GroupRepository groupRepository) {
        super(modelMapper, Educator.class, EducatorDto.class);
        this.personRepository = personRepository;
        this.groupRepository = groupRepository;
    }

    @PostConstruct
    public void setupMapper() {
        super.modelMapper.createTypeMap(Educator.class, EducatorDto.class)
                .addMappings(m -> m.skip(EducatorDto::setPersonId)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(EducatorDto::setGroupId)).setPostConverter(toDtoConverter());
        super.modelMapper.createTypeMap(EducatorDto.class, Educator.class)
                .addMappings(m -> m.skip(Educator::setPerson)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Educator::setGroup)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(EducatorDto source, Educator destination) {
        destination.setPerson(personRepository.findById(source.getPersonId())
                .orElseThrow(() -> new NotFoundException("Данные не найдены")));
        destination.setGroup(groupRepository.findById(source.getGroupId())
                .orElseThrow(() -> new NotFoundException("Группа не найдена")));
    }

    @Override
    protected void mapSpecificFields(Educator source, EducatorDto destination) {
        destination.setPersonId(source.getPerson().getId());
        destination.setGroupId(source.getGroup().getId());
    }
}

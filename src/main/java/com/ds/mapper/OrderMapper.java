package com.ds.mapper;

import com.ds.dto.OrderDto;
import com.ds.model.Order;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class OrderMapper extends GenericMapper<Order, OrderDto>{
    protected OrderMapper(ModelMapper modelMapper) {
        super(modelMapper, Order.class, OrderDto.class);
    }

    @Override
    protected void mapSpecificFields(OrderDto source, Order destination) {

    }

    @Override
    protected void mapSpecificFields(Order source, OrderDto destination) {

    }
}

package com.ds.mapper;

import com.ds.dto.GenericDto;
import com.ds.dto.UserDto;
import com.ds.model.GenericModel;
import com.ds.model.User;
import com.ds.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class UserMapper extends GenericMapper<User, UserDto>{

    private final UserRepository userRepository;
    protected UserMapper(ModelMapper modelMapper, UserRepository userRepository) {
        super(modelMapper, User.class, UserDto.class);
        this.userRepository = userRepository;
    }


    @Override
    protected void mapSpecificFields(UserDto source, User destination) {

    }

    @Override
    protected void mapSpecificFields(User source, UserDto destination) {

    }
}

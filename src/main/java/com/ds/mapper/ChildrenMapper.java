package com.ds.mapper;


import com.ds.dto.ChildrenDto;
import com.ds.model.Children;
import com.ds.repository.GroupRepository;
import com.ds.repository.OfficialRepresentativeRepository;
import com.ds.repository.PersonRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;

@Component
public class ChildrenMapper extends GenericMapper<Children, ChildrenDto> {

    private final PersonRepository personRepository;
    private final OfficialRepresentativeRepository officialRepresentativeRepository;
    private final GroupRepository groupRepository;

    protected ChildrenMapper(ModelMapper modelMapper, PersonRepository personRepository, OfficialRepresentativeRepository officialRepresentativeRepository, GroupRepository groupRepository) {
        super(modelMapper, Children.class, ChildrenDto.class);
        this.personRepository = personRepository;
        this.officialRepresentativeRepository = officialRepresentativeRepository;
        this.groupRepository = groupRepository;
    }

    @PostConstruct
    public void setupMapper() {
        super.modelMapper.createTypeMap(Children.class, ChildrenDto.class)
                .addMappings(m -> m.skip(ChildrenDto::setOfficialRepresentativeId)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(ChildrenDto::setGroupId)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(ChildrenDto::setPersonId)).setPostConverter(toDtoConverter());

        super.modelMapper.createTypeMap(ChildrenDto.class, Children.class)
                .addMappings(m -> m.skip(Children::setOfficialRepresentative)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Children::setPerson)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Children::setGroup)).setPostConverter(toEntityConverter());


    }

    @Override
    protected void mapSpecificFields(ChildrenDto source, Children destination) {
        destination.setPerson(personRepository.findById(source.getPersonId())
                .orElseThrow(() -> new NotFoundException("Данные не найдены")));
        destination.setOfficialRepresentative(officialRepresentativeRepository
                .findById(source.getOfficialRepresentativeId()).orElseThrow(() -> new NotFoundException("Официальный представитель не найден")));
        destination.setGroup(groupRepository.findById(source.getGroupId())
                .orElseThrow(() -> new NotFoundException("Группа не найдена")));
    }

    @Override
    protected void mapSpecificFields(Children source, ChildrenDto destination) {
        destination.setPersonId(source.getPerson().getId());
        destination.setOfficialRepresentativeId(source.getOfficialRepresentative().getId());
        destination.setGroupId(source.getGroup().getId());
    }
}

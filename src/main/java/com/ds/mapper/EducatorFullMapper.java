package com.ds.mapper;

import com.ds.dto.EducatorFullDto;
import com.ds.dto.GroupDto;
import com.ds.dto.PersonDto;
import com.ds.model.Educator;
import com.ds.repository.GroupRepository;
import com.ds.repository.PersonRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;

@Component
public class EducatorFullMapper extends GenericMapper<Educator, EducatorFullDto> {
    private final ModelMapper mapper;
    private final PersonRepository personRepository;
    private final GroupRepository groupRepository;

    public EducatorFullMapper(ModelMapper modelMapper, ModelMapper mapper,
                              PersonRepository personRepository, GroupRepository groupRepository) {
        super(modelMapper, Educator.class, EducatorFullDto.class);
        this.mapper = mapper;
        this.personRepository = personRepository;
        this.groupRepository = groupRepository;
    }

    @PostConstruct
    public void setupMapper() {
        mapper.createTypeMap(Educator.class, EducatorFullDto.class)
                .addMappings(m -> m.skip(EducatorFullDto::setPersonId))
                .addMappings(m -> m.skip(EducatorFullDto::setGroupId))
                .setPostConverter(toDtoConverter());
        mapper.createTypeMap(EducatorFullDto.class, Educator.class)
                .addMappings(m -> m.skip(Educator::setPerson))
                .addMappings(m -> m.skip(Educator::setGroup))
                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(EducatorFullDto source, Educator destination) {
        destination.setPerson(personRepository.findById(source.getPersonId())
                .orElseThrow(() -> new NotFoundException("Данные Person не найдены")));
        destination.setGroup(groupRepository.findById(source.getGroupId())
                .orElseThrow(() -> new NotFoundException("Данные Group не найдены")));
    }

    @Override
    protected void mapSpecificFields(Educator source, EducatorFullDto destination) {
        destination.setPersonId(source.getPerson().getId());
        destination.setPersonDto(modelMapper.map(source.getPerson(), PersonDto.class));
        destination.setGroupId(source.getGroup().getId());
        destination.setGroupDto(modelMapper.map(source.getGroup(), GroupDto.class));
    }
}

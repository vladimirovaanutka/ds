package com.ds.mapper;

import com.ds.dto.*;
import com.ds.model.Order;
import com.ds.repository.EducatorRepository;
import com.ds.repository.GoodTypeRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;

import java.util.HashSet;
import java.util.Objects;


@Component
public class OrderFullMapper extends GenericMapper<Order, OrderFullDto>{
    private final ModelMapper mapper;
    private final GoodTypeRepository goodTypeRepository;
//    private final EducatorRepository educatorRepository;

    protected OrderFullMapper(ModelMapper modelMapper, ModelMapper mapper, GoodTypeRepository goodTypeRepository, EducatorRepository educatorRepository) {
        super(modelMapper, Order.class, OrderFullDto.class);
        this.mapper = mapper;
        this.goodTypeRepository = goodTypeRepository;
//        this.educatorRepository = educatorRepository;
    }

    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Order.class, OrderFullDto.class)
                //.addMappings(m -> m.skip(OrderFullDto::setEducatorId)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(OrderFullDto::setGoodTypeId)).setPostConverter(toDtoConverter());

        modelMapper.createTypeMap(OrderFullDto.class, Order.class)
                //.addMappings(m -> m.skip(Order::setEducator)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Order::setGoodType)).setPostConverter(toEntityConverter());
    }



    @Override
    protected void mapSpecificFields(OrderFullDto source, Order destination) {
        destination.setGoodType(goodTypeRepository.findById(source.getGoodTypeId())
                .orElseThrow(() -> new NotFoundException("Данные GoodType не найдены")));
//        destination.setEducator(educatorRepository.findById(source.getEducatorId())
//                .orElseThrow(() -> new NotFoundException("Данные Educator не найдены")));

    }


    @Override
    protected void mapSpecificFields(Order source, OrderFullDto destination) {
        destination.setGoodTypeId(source.getGoodType().getId());
        destination.setGoodTypeDto(modelMapper.map(source.getGoodType(), GoodTypeDto.class));
//        destination.setEducatorId(source.getEducator().getId());
//        destination.setEducatorDto(modelMapper.map(source.getEducator(), EducatorDto.class));
        }
}

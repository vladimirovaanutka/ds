package com.ds.mapper;

import com.ds.dto.GoodTypeDto;
import com.ds.dto.StorageFullDto;
import com.ds.model.Storage;
import com.ds.repository.GoodTypeRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;

@Component
public class StorageFullMapper extends GenericMapper<Storage, StorageFullDto> {
    private final ModelMapper mapper;
    private final GoodTypeRepository goodTypeRepository;

    public StorageFullMapper(ModelMapper modelMapper, ModelMapper mapper, GoodTypeRepository goodTypeRepository) {
        super(modelMapper, Storage.class, StorageFullDto.class);
        this.mapper = mapper;
        this.goodTypeRepository = goodTypeRepository;
    }
    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Storage.class, StorageFullDto.class)
                .addMappings(m -> m.skip(StorageFullDto::setGoodTypeId)).setPostConverter(toDtoConverter());

        modelMapper.createTypeMap(StorageFullDto.class, Storage.class)
                .addMappings(m -> m.skip(Storage::setGoodType)).setPostConverter(toEntityConverter());
    }
    @Override
    protected void mapSpecificFields(StorageFullDto source, Storage destination) {
        destination.setGoodType(goodTypeRepository.findById(source.getGoodTypeId())
                .orElseThrow(() -> new NotFoundException("Данные GoodType не найдены")));
    }

    @Override
    protected void mapSpecificFields(Storage source, StorageFullDto destination) {
        destination.setGoodTypeId(source.getGoodType().getId());
        destination.setGoodTypeDto(modelMapper.map(source.getGoodType(), GoodTypeDto.class));
    }
}

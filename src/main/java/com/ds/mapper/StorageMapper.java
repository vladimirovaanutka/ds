package com.ds.mapper;

import com.ds.dto.StorageDto;
import com.ds.model.Storage;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class StorageMapper extends GenericMapper<Storage, StorageDto> {
    public StorageMapper(ModelMapper modelMapper) {
        super(modelMapper, Storage.class, StorageDto.class);
    }

    @Override
    protected void mapSpecificFields(StorageDto source, Storage destination) {

    }

    @Override
    protected void mapSpecificFields(Storage source, StorageDto destination) {

    }
}

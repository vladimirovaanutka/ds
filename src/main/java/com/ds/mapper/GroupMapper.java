package com.ds.mapper;

import com.ds.dto.GroupDto;
import com.ds.model.GenericModel;
import com.ds.model.Group;
import com.ds.repository.ChildrenRepository;
import com.ds.repository.EducatorRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class GroupMapper extends GenericMapper<Group, GroupDto>{

    protected GroupMapper(ModelMapper modelMapper) {
        super(modelMapper, Group.class, GroupDto.class);
    }

    @Override
    protected void mapSpecificFields(GroupDto source, Group destination) {
    }

    @Override
    protected void mapSpecificFields(Group source, GroupDto destination) {
    }
}

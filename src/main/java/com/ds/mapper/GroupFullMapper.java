package com.ds.mapper;

import com.ds.dto.ChildrenDto;
import com.ds.dto.EducatorFullDto;
import com.ds.dto.GroupFullDto;
import com.ds.model.GenericModel;
import com.ds.model.Group;
import com.ds.repository.ChildrenRepository;
import com.ds.repository.EducatorRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class GroupFullMapper extends GenericMapper<Group, GroupFullDto>{
    private final ModelMapper mapper;
    private final EducatorRepository educatorRepository;
    private final ChildrenRepository childrenRepository;

    public GroupFullMapper(ModelMapper modelMapper, ModelMapper mapper, EducatorRepository educatorRepository, ChildrenRepository childrenRepository) {
        super(modelMapper, Group.class, GroupFullDto.class);
        this.mapper = mapper;
        this.educatorRepository = educatorRepository;
        this.childrenRepository = childrenRepository;
    }
    @PostConstruct
    public void setupMapper() {
        modelMapper.createTypeMap(Group.class, GroupFullDto.class)
                .addMappings(m -> m.skip(GroupFullDto::setChildrenIds)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(GroupFullDto::setEducatorIds)).setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(GroupFullDto.class, Group.class)
                .addMappings(m -> m.skip(Group::setChildrenSet)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Group::setEducatorSet)).setPostConverter(toEntityConverter());
    }
    @Override
    protected void mapSpecificFields(GroupFullDto source, Group destination) {
        if(!Objects.isNull(source.getChildrenIds())) {
            destination.setChildrenSet(new HashSet<>(childrenRepository.findAllById(source.getChildrenIds())));
        }
        if(!Objects.isNull(source.getEducatorIds())) {
            destination.setEducatorSet(new HashSet<>(educatorRepository.findAllById(source.getEducatorIds())));
        }
    }

    @Override
    protected void mapSpecificFields(Group source, GroupFullDto destination) {
        destination.setChildrenIds(getChildrenIds(source));
        destination.setChildrenDtos(getChildrenDtos(source));
        destination.setEducatorIds(getEducatorIds(source));
        destination.setEducatorDtos(getEducatorDtos(source));
    }

    private Set<ChildrenDto> getChildrenDtos(Group entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getChildrenSet())
                ? null
                : entity.getChildrenSet()
                .stream()
                .map(x -> modelMapper.map(x, ChildrenDto.class))
                .collect(Collectors.toSet());
    }

    private Set<EducatorFullDto> getEducatorDtos(Group entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getEducatorSet())
                ? null
                : entity.getEducatorSet()
                .stream()
                .map(x -> modelMapper.map(x, EducatorFullDto.class))
                .collect(Collectors.toSet());
    }

    protected Set<Integer> getEducatorIds(Group entity) {
            return Objects.isNull(entity) || Objects.isNull(entity.getEducatorSet())
                    ? null
                    : entity.getEducatorSet()
                    .stream()
                    .map(GenericModel::getId)
                    .collect(Collectors.toSet());
        }

    private Set<Integer> getChildrenIds(Group entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getChildrenSet())
                ? null
                : entity.getChildrenSet()
                .stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }

}

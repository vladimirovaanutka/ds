package com.ds.constants;

public interface UserRolesConstants {
    String ADMIN = "Администратор";
    String EDUCATOR = "Воспитатель";
}

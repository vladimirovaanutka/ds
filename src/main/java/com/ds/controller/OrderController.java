package com.ds.controller;

import com.ds.dto.OrderDto;
import com.ds.mapper.OrderMapper;
import com.ds.model.Order;
import com.ds.service.OrderService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
@Tag(name = "Заказ", description = "Контроллер для работы с заказами воспитателей")
public class OrderController extends GenericController<Order, OrderDto> {
    private final OrderService orderService;
    private final OrderMapper orderMapper;

    public OrderController(OrderService orderService, OrderMapper orderMapper) {
        super(orderService, orderMapper);
        this.orderService = orderService;
        this.orderMapper = orderMapper;
    }
}

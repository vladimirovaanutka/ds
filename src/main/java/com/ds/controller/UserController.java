package com.ds.controller;

import com.ds.dto.UserDto;
import com.ds.mapper.UserMapper;
import com.ds.model.User;
import com.ds.service.UserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
@Tag(name = "Пользователь", description = "Контроллер для работы с пользователями")
public class UserController extends GenericController<User, UserDto> {
    private final UserService userService;
    private final UserMapper userMapper;

    public UserController(UserService userService, UserMapper userMapper) {
        super(userService, userMapper);
        this.userService = userService;
        this.userMapper = userMapper;
    }
}

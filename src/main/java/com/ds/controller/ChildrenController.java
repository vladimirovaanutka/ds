package com.ds.controller;

import com.ds.dto.ChildrenDto;
import com.ds.mapper.ChildrenMapper;
import com.ds.model.Children;
import com.ds.service.ChildrenService;
import com.ds.service.OfficialRepresentativeService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/children")
@Tag(name = "Дети", description = "Контроллер для работы с детьми")
public class ChildrenController extends GenericController<Children, ChildrenDto> {
    private final OfficialRepresentativeService officialRepresentativeService;
    private final ChildrenMapper childrenMapper;
    private final ChildrenService childrenService;

    public ChildrenController(OfficialRepresentativeService officialRepresentativeService, ChildrenMapper childrenMapper, ChildrenService childrenService) {
        super(childrenService, childrenMapper);
        this.officialRepresentativeService = officialRepresentativeService;
        this.childrenMapper = childrenMapper;
        this.childrenService = childrenService;
    }
}

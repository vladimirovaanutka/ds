package com.ds.controller;

import com.ds.dto.EducatorDto;
import com.ds.mapper.EducatorMapper;
import com.ds.model.Educator;
import com.ds.service.EducatorService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/educator")
@Tag(name = "Воспитатель", description = "Контроллер для работы с воспитателями")
public class EducatorController extends GenericController<Educator, EducatorDto> {
    private final EducatorMapper educatorMapper;
    private final EducatorService educatorService;

    public EducatorController(EducatorMapper educatorMapper, EducatorService educatorService) {
        super(educatorService, educatorMapper);
        this.educatorMapper = educatorMapper;
        this.educatorService = educatorService;
    }
}

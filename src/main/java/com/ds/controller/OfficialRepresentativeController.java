package com.ds.controller;

import com.ds.dto.OfficialRepresentativeDto;
import com.ds.mapper.OfficialRepresentativeMapper;
import com.ds.model.Children;
import com.ds.model.OfficialRepresentative;
import com.ds.service.ChildrenService;
import com.ds.service.OfficialRepresentativeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.webjars.NotFoundException;

@RestController
@RequestMapping("/official_representative")
@Tag(name = "Официальный представитель", description = "Контроллер для работы с официальными представителями")
public class OfficialRepresentativeController extends GenericController<OfficialRepresentative, OfficialRepresentativeDto> {

    private final OfficialRepresentativeService officialRepresentativeService;
    private final ChildrenService childrenService;
    private final OfficialRepresentativeMapper officialRepresentativeMapper;

    public OfficialRepresentativeController(OfficialRepresentativeService officialRepresentativeService, ChildrenService childrenService, OfficialRepresentativeMapper officialRepresentativeMapper) {
        super(officialRepresentativeService, officialRepresentativeMapper);
        this.officialRepresentativeService = officialRepresentativeService;
        this.childrenService = childrenService;
        this.officialRepresentativeMapper = officialRepresentativeMapper;
    }

    @Operation(description = "Добавить ребенка к официальному представителю", method = "addChildren")
    @RequestMapping(value = "/addChildren", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OfficialRepresentative> addChildren(@RequestParam(value = "childrenId") Integer childrenId,
                                                              @RequestParam(value = "officialRepresentativeId") Integer officialRepresentativeId) {
        try {
            Children children = childrenService.getOne(childrenId);
            OfficialRepresentative officialRepresentative = (OfficialRepresentative) officialRepresentativeService.getOne(officialRepresentativeId);
            children.setOfficialRepresentative(officialRepresentative);

            return ResponseEntity.status(HttpStatus.OK).body(officialRepresentative);
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }
}

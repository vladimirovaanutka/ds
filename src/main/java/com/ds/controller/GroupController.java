package com.ds.controller;

import com.ds.dto.GroupDto;
import com.ds.mapper.GroupMapper;
import com.ds.model.Children;
import com.ds.model.Educator;
import com.ds.model.Group;
import com.ds.service.ChildrenService;
import com.ds.service.EducatorService;
import com.ds.service.GroupService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.webjars.NotFoundException;
import org.springframework.http.HttpStatus;
@RestController
@RequestMapping("/group")
@Tag(name = "Группа", description = "Контроллер для работы с группами")
public class GroupController extends GenericController<Group, GroupDto>{
    private final GroupMapper groupMapper;
    private final ChildrenService childrenService;
    private final GroupService groupService;
    private final EducatorService educatorService;

    public GroupController(GroupMapper groupMapper, ChildrenService childrenService, EducatorService educatorService, GroupService groupService) {
        super(groupService, groupMapper);
        this.groupMapper = groupMapper;
        this.childrenService = childrenService;
        this.groupService = groupService;
        this.educatorService = educatorService;

    }


    @Operation(description = "Добавить ребенка в группу", method = "addChildren")
    @RequestMapping(value = "/addChildren", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GroupDto> addChildren(@RequestParam(value = "childrenId") Integer childrenId,
                                             @RequestParam(value = "groupId") Integer groupId) {
        try {
            Children children = childrenService.getOne(childrenId);
            Group group = groupService.getOne(groupId);
            group.getChildrenSet().add(children);

            return ResponseEntity.status(HttpStatus.OK).body(groupMapper.toDto(groupService.update(group)));
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }
    @Operation(description = "Добавить воспитателя в группу", method = "addEducator")
    @RequestMapping(value = "/addEducator", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Group> addEducator(@RequestParam(value = "educatorId") Integer educatorId,
                                             @RequestParam(value = "groupId") Integer groupId) {
        try {
            Educator educator = educatorService.getOne(educatorId);
            Group group = groupService.getOne(groupId);
//            group.getEducatorSet().add(educator);

            return ResponseEntity.status(HttpStatus.OK).body(group);
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

}

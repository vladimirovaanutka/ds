package com.ds.repository;

import com.ds.model.Storage;
import org.springframework.stereotype.Repository;

@Repository
public interface StorageRepository extends GenericRepository <Storage> {
}

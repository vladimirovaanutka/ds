package com.ds.repository;

import com.ds.model.OfficialRepresentative;
import com.ds.model.Person;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OfficialRepresentativeRepository extends GenericRepository<OfficialRepresentative> {

    @Query("select o from OfficialRepresentative o where o.person.id = :id and o.person.isDeleted=false and o.person.isChild=false")
    List<OfficialRepresentative> getOfficialRepresentativeListByPersonId(Integer id);
}

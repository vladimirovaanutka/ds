package com.ds.repository;

import com.ds.model.GoodType;
import org.springframework.stereotype.Repository;

@Repository
public interface GoodTypeRepository extends GenericRepository<GoodType>{
}

package com.ds.repository;

import com.ds.model.GenericModel;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

@NoRepositoryBean
@Hidden
public interface GenericRepository<T extends GenericModel> extends JpaRepository<T, Integer> {
    List<T> findByCreatedBy(String createdBy);

    @Override
    @Query("select e from #{#entityName} e where e.isDeleted=false")
    public Page<T> findAll(Pageable pageable);

    @Override
    @Query("select e from #{#entityName} e where e.isDeleted=false")
    List<T> findAll();
}

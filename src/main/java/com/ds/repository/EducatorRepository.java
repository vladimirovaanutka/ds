package com.ds.repository;

import com.ds.model.Educator;
import com.ds.model.Person;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EducatorRepository extends GenericRepository<Educator> {
}

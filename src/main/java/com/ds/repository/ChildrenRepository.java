package com.ds.repository;

import com.ds.model.Children;
import org.springframework.stereotype.Repository;

@Repository

public interface ChildrenRepository extends GenericRepository<Children>{

}

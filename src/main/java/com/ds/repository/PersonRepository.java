package com.ds.repository;

import com.ds.model.Person;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepository extends GenericRepository<Person>{

    @Query("select p from Person p where p.isChild = false and p.isDeleted=false")
    List<Person> getAdult();

    @Query("""
            select case when count(p) > 0 then false else true end
            from Person p
                join Educator e on p.id = e.person.id
                join OfficialRepresentative o on p.id = o.person.id
             where p.id = :id and e.isDeleted=false and o.isDeleted=false 
             """)
    Boolean isAdultPersonCanDelete(Integer id);

}

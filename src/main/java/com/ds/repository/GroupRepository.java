package com.ds.repository;

import com.ds.model.Group;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends GenericRepository<Group> {
}

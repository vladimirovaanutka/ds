package com.ds.dto;

import com.ds.model.enums.Gender;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ChildrenDto extends GenericDto {
    private Integer personId;
    private Integer groupId;
    private Integer officialRepresentativeId;
}

package com.ds.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderFullDto extends OrderDto{
    private GoodTypeDto goodTypeDto;
//    private EducatorDto educatorDto;
}

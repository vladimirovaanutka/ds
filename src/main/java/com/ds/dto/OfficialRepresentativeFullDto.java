package com.ds.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class OfficialRepresentativeFullDto extends OfficialRepresentativeDto {
    private PersonDto personDto;
}

package com.ds.dto;


import com.ds.model.enums.GroupType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GroupDto extends GenericDto {
    private String name;
    private Set<Integer> educatorIds;
    private Set<EducatorFullDto> educatorDtos;
    private GroupType groupType;
    private Set<Integer> childrenIds;
    private Set<ChildrenDto> childrenDtos;
}

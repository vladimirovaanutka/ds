package com.ds.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ChildrenFullDto extends ChildrenDto {
    private PersonDto personDto;
    private GroupDto groupDto;
    private OfficialRepresentativeFullDto officialRepresentativeDto;
    private Integer officialRepresentativePersonId;
}

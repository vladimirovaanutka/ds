package com.ds.dto;

import com.ds.model.enums.Gender;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PersonDto extends GenericDto{
    private String name;
    private String surname;
    private String patronymic;
    private Gender gender;
    private LocalDate birthDate;
    private String phone;
    private Boolean isChild;
}

package com.ds.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class OfficialRepresentativeDto extends GenericDto {
    private Integer personId;
    private Set<Integer> childrensIds;
    private String address;
    private String email;
}

package com.ds.dto;

import jdk.jfr.Percentage;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreatePersonDto {
    private String redirect;
    private String pageName;
    private PersonDto person = new PersonDto();
}

package com.ds.dto;

import com.ds.model.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class OrderDto extends GenericDto {
//    private Integer educatorId;
    private Integer goodTypeId;
    private String description;
    private Status status;
}

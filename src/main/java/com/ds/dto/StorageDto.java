package com.ds.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class StorageDto extends GenericDto {
    private Integer goodTypeId;
    private String name;

    private Integer quantity;

    private String comments;
}

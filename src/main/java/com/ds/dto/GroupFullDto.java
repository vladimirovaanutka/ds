package com.ds.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GroupFullDto extends GroupDto {
    private EducatorDto educatorDto;
    private ChildrenDto childrenDto;

}

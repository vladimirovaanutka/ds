package com.ds.service;

import com.ds.model.GoodType;
import com.ds.repository.GenericRepository;
import com.ds.repository.GoodTypeRepository;
import org.springframework.stereotype.Service;

@Service
public class GoodTypeService extends GenericService<GoodType>{
    private final GoodTypeRepository goodTypeRepository;

    protected GoodTypeService(GoodTypeRepository goodTypeRepository) {
        super(goodTypeRepository);
        this.goodTypeRepository = goodTypeRepository;
    }
}

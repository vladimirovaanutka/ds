package com.ds.service;

import com.ds.model.Educator;
import com.ds.repository.EducatorRepository;
import org.springframework.stereotype.Service;

@Service
public class EducatorService extends GenericService<Educator> {
    private final EducatorRepository educatorRepository;

    public EducatorService(EducatorRepository educatorRepository) {
        super(educatorRepository);
        this.educatorRepository = educatorRepository;
    }
}

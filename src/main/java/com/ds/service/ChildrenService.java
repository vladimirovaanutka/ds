
package com.ds.service;

import com.ds.model.Children;
import com.ds.repository.ChildrenRepository;
import com.ds.repository.GenericRepository;
import org.springframework.stereotype.Service;

@Service
public class ChildrenService extends GenericService<Children> {

    private final ChildrenRepository childrenRepository;
    private final PersonService personService;

    public ChildrenService(ChildrenRepository childrenRepository, PersonService personService) {
        super(childrenRepository);
        this.childrenRepository = childrenRepository;
        this.personService = personService;
    }

    public String getFioById(Integer id) {
        Children children = getOne(id);
        return personService.getFioById(children.getPerson().getId());
    }


}


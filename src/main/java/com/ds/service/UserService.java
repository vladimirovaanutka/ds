package com.ds.service;

import com.ds.model.User;
import com.ds.repository.GenericRepository;
import com.ds.repository.OrderRepository;
import com.ds.repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserService extends GenericService<User> {

    private final UserRepository userRepository;

    protected UserService(UserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }
}

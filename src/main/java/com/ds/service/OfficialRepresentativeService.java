package com.ds.service;


import com.ds.dto.OfficialRepresentativeDto;
import com.ds.mapper.OfficialRepresentativeMapper;
import com.ds.model.OfficialRepresentative;
import com.ds.repository.OfficialRepresentativeRepository;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;

@Service
public class OfficialRepresentativeService extends GenericService<OfficialRepresentative> {

    private final OfficialRepresentativeRepository officialRepresentativeRepository;
    private final OfficialRepresentativeMapper officialRepresentativeMapper;

    protected OfficialRepresentativeService(OfficialRepresentativeRepository officialRepresentativeRepository,
                                            OfficialRepresentativeMapper officialRepresentativeMapper) {
        super(officialRepresentativeRepository);
        this.officialRepresentativeRepository = officialRepresentativeRepository;
        this.officialRepresentativeMapper = officialRepresentativeMapper;
    }

    public OfficialRepresentative getOfficialRepresentativeByPersonId(Integer personId) {
        List<OfficialRepresentative> officialRepresentatives =
                officialRepresentativeRepository.getOfficialRepresentativeListByPersonId(personId);
        if (officialRepresentatives == null || officialRepresentatives.isEmpty()) {
            OfficialRepresentativeDto officialRepresentativeDto = new OfficialRepresentativeDto();
            officialRepresentativeDto.setPersonId(personId);
            return create(officialRepresentativeMapper.toEntity(officialRepresentativeDto));
        }
        if (officialRepresentatives.size() > 1) {
            throw new IllegalArgumentException(" Некорректные данные в базе официальных представителей");
        }

        return officialRepresentatives.get(0);
    }
}

package com.ds.service;

import com.ds.model.Storage;
import com.ds.repository.StorageRepository;
import org.springframework.stereotype.Service;

@Service
public class StorageService extends GenericService<Storage> {
    private final StorageRepository storageRepository;

    protected StorageService(StorageRepository storageRepository) {
        super(storageRepository);
        this.storageRepository = storageRepository;
    }
}

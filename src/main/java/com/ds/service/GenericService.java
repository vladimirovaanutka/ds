package com.ds.service;

import com.ds.model.GenericModel;
import com.ds.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.webjars.NotFoundException;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
public abstract class GenericService<T extends GenericModel> {
    private final GenericRepository<T> repository;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    protected GenericService(GenericRepository<T> repository) {
        this.repository = repository;
    }

    public List<T> listAll() {
        return repository.findAll();
    }

    public T getOne(Integer id) {
        return repository.findById(id).orElseThrow(() -> new NotFoundException("Запись с таким id не найдена"));
    }

    public T create(T object) {
        object.setCreatedBy("ADMIN");
        object.setCreatedWhen(LocalDateTime.now());
        return repository.save(object);
    }

    public T update(T object) {
        return repository.save(object);
    }

    public void delete(Integer id) {
        repository.deleteById(id);
    }

    public List<T> findByCreatedBy(String createdBy) {
        return repository.findByCreatedBy(createdBy);
    }

    public void softDelete(Integer id) {
        log.error("GENERIC SOFT");
        T object = getOne(id);
//        object.setDeletedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        object.setDeletedBy("ADMIN");
        object.setDeleted(true);
        object.setDeletedWhen(LocalDateTime.now());
        update(object);
    }

    public void restore(Integer id) {
        T object = getOne(id);
//        object.setDeletedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        object.setDeletedBy("ADMIN");
        object.setDeleted(false);
        object.setDeletedWhen(null);
        update(object);
    }

    public boolean existsById(Integer id) {
        return repository.existsById(id);
    }

    public Page<T> listAll(Pageable pageable) {
        return repository.findAll(pageable);
        //return repository.findAll();
    }
}


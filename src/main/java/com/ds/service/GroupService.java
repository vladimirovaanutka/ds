package com.ds.service;

import com.ds.model.Group;
import com.ds.repository.GroupRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class GroupService extends GenericService<Group> {
    private final GroupRepository groupRepository;

    public GroupService(GroupRepository groupRepository) {
        super(groupRepository);
        this.groupRepository = groupRepository;
    }
}

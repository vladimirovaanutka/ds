package com.ds.service;

import com.ds.model.Order;
import com.ds.repository.GenericRepository;
import com.ds.repository.OfficialRepresentativeRepository;
import com.ds.repository.OrderRepository;
import org.springframework.stereotype.Service;

@Service
public class OrderService extends GenericService<Order> {

    private final OrderRepository orderRepository;

    protected OrderService(OrderRepository orderRepository) {
        super(orderRepository);
        this.orderRepository = orderRepository;
    }
}

package com.ds.service;

import com.ds.dto.PersonDto;
import com.ds.model.Person;
import com.ds.repository.PersonRepository;
import com.sun.source.doctree.IndexTree;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonService extends GenericService<Person>{
    private final PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        super(personRepository);
        this.personRepository = personRepository;
    }

    public List<Person> getAdultPersons() {
        return personRepository.getAdult();
    }

    public String getFio(PersonDto personDto) {
        if (personDto == null) {
            return "";
        }
        String fio = personDto.getSurname() + " " + personDto.getName() ;
        if (StringUtils.isNotBlank(personDto.getPatronymic())) {
            fio += " " + personDto.getPatronymic();
        }
        return fio;
    }

    public String getFioById(Integer id) {
        if (id == null) {
            return "";
        }

        Person person = getOne(id);
        String fio = person.getSurname() + " " + person.getName() ;
        if (StringUtils.isNotBlank(person.getPatronymic())) {
            fio += " " + person.getPatronymic();
        }
        return fio;
    }

    public boolean isAdultPersonCanDelete(PersonDto personDto) {
        return personRepository.isAdultPersonCanDelete(personDto.getId());
    }
}
